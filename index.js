console.log('1st Code');

function getDiff(a1,a2){
    return  a1.filter(function(n) { return a2.indexOf(n) == -1;});
}

// Insert two sets of arrays here..
var str1 = [...'ABC'];
var str2 = [...'BC'];

var str3 = [...'BC'];
var str4 = [...'BANGALORE'];

var op1=getDiff(str1, str2);
var op2=getDiff(str2, str1);
var op3=getDiff(str3, str4);
var op4=getDiff(str4, str3);

console.log("Array 1 Diff");
console.log(op1);
console.log(op2);
console.log("Array 2 Diff");
console.log(op3);
console.log(op4);


console.log('2nd Code')

// Insert Multidimensional Array
arr = [["U1","U2"], ["U3","U4"], ["U1","U5"], ["U1","U2"], ["U3","U4"]];

function friendsArrRemover(arr) {
    var a = [];
    var itemsFoundArr = {};
    for(var i = 0, l = arr.length; i < l; i++) {
        var stringified = JSON.stringify(arr[i]);
        if(itemsFoundArr[stringified]) { continue; }
        a.push(arr[i]);
        itemsFoundArr[stringified] = true;
    }
    return a;
}

friendsArrRemover(arr);